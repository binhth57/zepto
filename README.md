#
Frontend Starter

This is frontend starter template for the html team

## Requirements

* Nodejs \([https://nodejs.org/en/download/](https://nodejs.org/en/download/)\)
* On Windows - Install C++ Build Tools for Windows \(run command with administrator\)

```sh
npm install --global --production windows-build-tools
```

* Node.js native addon build tool \([https://github.com/nodejs/node-gyp](https://github.com/nodejs/node-gyp)\)

* Install the `Gulp CLI` command \(optional\):
\`\`\`sh
```
npm install --global gulp-cli
```

## Install
```sh
git clone https://git.9wdev.com/9thWonder/c8starter.git <projectname>
cd <projectname>
branch html
npm install

## Configuring the git and githook

###### Changing the origin

```sh
git remote set-url origin https://git.9wdev.com/9thWonder/<NEW_GIT_REPO>.git
git checkout -b html
```

###### Installing the githook \(comming soon\)

The `husky` module will install git hook for you and when you run git command, the husky will run the npm script for you

```sh
npm install husky --save-dev
```

## Development

###### Starting the server in development mode

```sh
npm start
gulp
```

###### Generating pages

```sh
# creating `src/html/{about-us.html, contact.html}`
npm run create-page -- -p 'about-us contact'
```

###### Generating modules \(html and css\)

```sh
# creating `src/html/modules/mod-banner.html` `src/assets/stylesheets/module/mod-banner.scss`
# and `src/html/modules/mod-cta.html` `src/assets/stylesheets/module/mod-cta.scss`
npm run create-module -- -m 'mod-banner mod-cta' # seperating by comma
```

###### Generating module javascript

```sh
# creating `src/assets/javascripts/modules/modBanner.js`
npm run create-js -- -m 'mod-banner' # seperating by comma
```

## Production

###### Build

```sh
# build all image/fonts/js/css/html
npm run build
gulp build

# only build image
npm run img
gulp img

# only build html
npm run html
gulp html

# only build css
npm run css
gulp css

# only build js
npm run js
gulp css


# starting demo server
'npm run demo' after run cmd 'npm run build' or run cmd 'gulp production'
```

## Pushing to the git.9wdev.com

##### \#

```sh
git add .
git commit -m "message" # NOTE `PRECOMMIT HOOK` WILL RUN TO CHECK `JS LINT` AND `STYLELINT`
git push origin html
```

###### Build css inline

install node modules
```sh
npm i critical
```
find '/gulpfile.js/tasks/critical.js' and '/gulpfile.js/tasks/critical-inline.js'
open comment 'const critical = require('critical').stream'

run cmd build css inline
```sh
#  run cmd after build npm run build or npm run inline
npm run critical-inline 
```

#config visual studio code
- use editorconfig, stylelint, standard js format code.
- install extensions: Beautify, StandardJS, stylelint.

# chuẩn code Frontend
- tên các class, tên file vv (trừ js) đều viết thường.
- không css cho các class của bootstrap (add thêm class và style nó).
- selector css ko lồng nhiều cấp, nếu class mình đặt thì move nó ra ngoài cùng.
  tối đa 4 cấp.
- những css dùng chung style trong custom.scss or boostrap có class rồi thì nên dùng lại, trách code thêm gây trùng code.
- Những js dùng chung được viết trong UserAgent.js
- name trong file package.json và title trong global data phải được đổi theo tên của project.
- mobile/ipad không load animation/background-video.
- resize mobile lên desktop/desktop xuống mobile phải reload lại page.
- tách css/js theo mobile/desktop/page. 
  remove js/css không dùng trên các version.
- hình svg cần thêm width/height trong file tránh lỗi ie.
- tất cả image cần phải có alt/src.
- tất cả link đều phải thêm class sr-only vào span trong thẻ a nếu thẻ a đó ko có text.
  text bên trong mô tả chức năng or title của link đó.
- tất cả input phải có label đi kèm và label phải có text for trùng id của input đó.      nếu design ko có label thì thêm class sr-only.
- cần code module html theo module data js và page data js.
- fonts google cần thêm media media="screen and (max-width: 1px)" onload="if(media!='screen')media='screen'" có display swap đi kèm.
- fonts face phải có fonts-display

 => Guide https://9thwonder-my.sharepoint.com/:x:/p/duc_huynh/EQY5-LjCJGdLob_EBUYYyuQBU5ppBmoX4eipLmf0SekwAg?e=ybhkZB
 

 