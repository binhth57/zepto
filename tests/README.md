Frontend CI Performance check for Jenkins

Please make sure these package is installed in package.json
- cssstats

edit config.json for require speed of mobile site

performance.js : automation run lighthouse performance score check and alert if score bellow setting score

css.js : cssstats info
- add more css line if your project have more css files than default
processCSS('./public/stylesheets/app-desktop.css','./tests/reports/app-desktop.report');

report save to reports folder and will be archive as Artifacts. Login to jenkins to download that file

Edit Jenkinsfile to suite your need