var shell = require('shelljs');
var fs = require('fs');

const configVal = JSON.parse(fs.readFileSync('tests/config.json', 'utf8'));

var serverShell = shell.exec('http-server --silent ./public -p '+configVal.port+' ', function(code, stdout, stderr) {
});
 
 

shell.exec('docker run --net=host -v "$(pwd)"/tests/reports:/home/chrome/reports --cap-add=SYS_ADMIN 9thwondervn/automation:lighthouse lighthouse --chrome-flags="--headless --disable-gpu --no-sandbox" --output=html --output-path=/home/chrome/reports/lighthouse-results.html --output=json --no-enable-error-reporting --quiet http://localhost:'+configVal.port+'/'+configVal.url, function(code, stdout, stderr) {
     
    if(code===0){
        
        var obj = JSON.parse(fs.readFileSync('tests/reports/lighthouse-results.report.json', 'utf8'));
        var performanceScore = obj.categories.performance.score*100;
        console.log("PERFORMANCE SCORE : " + performanceScore );
        if( performanceScore < configVal.score){
            console.log("PERFORMANCE SLOW DETECTED");
            serverShell.kill('SIGINT');
            shell.exit(1);
        }else{
            serverShell.kill('SIGINT'); 
            shell.exit(0); 
        }
    }else{
        serverShell.kill('SIGINT');
        shell.exit(1);
    }
    
});

