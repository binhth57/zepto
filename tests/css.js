var fs = require('fs')
var cssstats = require('cssstats')

var app = {
  fs: require('fs'),
  cssstats: require('cssstats'),
  path: require('path'),
  q: require('q'),
  parseHtml: require('./libs/parse-html.js')
}

var options = {
  logConsole: false,
  jsonOutput: true,
  htmlOutput: true,

  fileSize: true,
  uniqueDeclarations: [
    'font-size',
    'float',
    'width',
    'height',
    'color',
    'background-color'
  ],
  addOrigin: true,
  addRawCss: false,
  addHtmlStyles: false,
  addGraphs: true,
  csslint: false
}

processCSS('./public/assets/stylesheets/app-desktop.css', './tests/reports/app-desktop.report')
processCSS('./public/assets/stylesheets/app.css', './tests/reports/app.report')

/*  UTILS FUNCTIONS FOR CSSSTATS HTML REPORT */

function processCSS (cssPath, reportPath) {
  var css = {
    concat: ''
  }

  css.concat = fs.readFileSync(cssPath, 'utf8')

  var destJson = reportPath + '.json'
  var destHtml = reportPath + '.html'

  var stats = parseStats(css)

  if (options.logConsole) {
    console.log(stats)
  }

  if (options.jsonOutput) {
    var statsString = JSON.stringify(stats, undefined, 4)
    fs.writeFileSync(destJson, statsString, 'utf8')
  }

  if (options.htmlOutput) {
    var output = app.parseHtml(app, options, stats, css)
    fs.writeFileSync(destHtml, output, 'utf8')
  }
}

function parseStats (cssObject) {
  var css = cssObject.concat

  var origin = app.cssstats(css)
  var stats = {
    fileSize: {
      size: getSizeUnit(origin.size),
      gzipSize: getSizeUnit(origin.gzipSize)
    },
    selectorSpecifity: {},

    totals: {
      rules: origin.rules.total,
      selectors: origin.selectors.total,
      declarations: origin.declarations.total,
      properties: 0,
      vendorPrefixes: origin.declarations.getVendorPrefixed().length,
      mediaQueries: origin.mediaQueries.total,
      importants: css.match(/!important/g || []) ? css.match(/!important/g || []).length : 0
    },

    uniqueDeclarations: {},

    selectors: {
      'type': origin.selectors.type,
      'class': origin.selectors.class,
      'id': origin.selectors.id,
      'pseudoClass': origin.selectors.pseudoClass,
      'pseudoElement': origin.selectors.pseudoElement
    },

    propertyResets: {},

    uniqueValues: {
      colors: getValueCount(origin.declarations.properties['color'], 'color'),
      backgroundColors: {},
      fontSizes: {},
      fontFamilies: getValueCount(origin.declarations.properties['font-family'], 'font-family')
    },

    uniqueMediaQueries: getValueCount(origin.mediaQueries.values, 'mediaQueries')
  }

  if (options.addGraphs) {
    stats.graphs = {}
    stats.graphs.selectorSpecificity = origin.selectors.getSpecificityGraph()
    stats.graphs.rulesetSize = origin.rules.size.graph
  }

  if (options.addRawCss) {
    stats.rawCss = css
  }

  if (options.addOrigin) {
    stats.origin = origin
  }

    // get all unique declarations
  for (var prop in origin.declarations.properties) {
    if (origin.declarations.properties.hasOwnProperty(prop)) {
      if (options.uniqueDeclarations.indexOf(prop) > -1) {
        stats.uniqueDeclarations[prop] = origin.declarations.properties[prop].length
      }
      stats.totals.properties = stats.totals.properties + 1
    }
  }
    // get colors from shorthand attribute 'background'
  var bgColors = origin.declarations.properties['background-color']
        ? origin.declarations.properties['background-color']
        : []

  if (origin.declarations.properties['background']) {
    origin.declarations.properties['background'].forEach(function (bg) {
      var bgColor = bg.match(/#[0-9a-fA-F]+|#[0-9]|#[A-F]+|#[a-f]+|(rgba|rgb)\([0-9,.]+?\)/g)

      if (bgColor) {
        bgColors.push(bgColor[0])
      }
    })
  }

  stats.uniqueValues.backgroundColors = getValueCount(bgColors, 'bgColors')

    // get font sizes from shorthand attribute 'font'
  var fontSizes = origin.declarations.properties['font-size']
        ? origin.declarations.properties['font-size']
        : []

  if (origin.declarations.properties['font']) {
    origin.declarations.properties['font'].forEach(function (f) {
      var fontSize = f.match(/([\d.])+ ?(em|ex|%|dpx|px|cm|mm|in|pt|pc|ch|rem|vh|vw|vmin|vmax)\/?[\d.]?|(inherit|small|smaller|big|bigger)/g)

      if (fontSize) {
        fontSizes.push(fontSize[0])
      }
    })
  }

  stats.uniqueValues.fontSizes = sortFontSizes(getValueCount(fontSizes), 'fontSizes')

  stats.propertyResets = origin.declarations.getPropertyResets()
  stats.selectorSpecifity = origin.selectors.specificity

  if (options.csslint) {
    var linted = app.parseCsslint(app, options.csslint, cssObject)

    if (linted) {
      stats.lint = linted
    }
  }

  return stats
}

function getSizeUnit (size) {
  var kbFactor = 1024
  var mbFactor = kbFactor * 1024
  var gbFactor = mbFactor * 1024
  var tbFactor = gbFactor * 1024

  if ((size / kbFactor) < 1024) {
    var r = (size / kbFactor).toFixed(2) + ' KB'
  } else if ((size / mbFactor) < 1024) {
    var r = (size / mbFactor).toFixed(2) + ' MB'
  } else if ((size / gbFactor) < 1024) {
    var r = (size / gbFactor).toFixed(2) + ' GB'
  } else if ((size / tbFactor) < 1024) {
    var r = (size / tbFactor).toFixed(2) + ' TB'
  } else {
    var r = (size).toFixed(2) + ' B'
  }

  return r
}

function sortFontSizes (fontSizes) {
  var r = {}
  var temp = []
  var size = 0
  var biggest = 0

  for (var prop in fontSizes) {
    if (fontSizes.hasOwnProperty(prop)) {
      size++

      var val = prop.replace(/[^0-9.\/]/g, '').replace(/\/[0-9]/g, '')

      if (prop.indexOf('%') > -1) {
        val = 16 / 100 * val
      }

      if (prop.indexOf('em') > -1 ||
                prop.indexOf('rem') > -1 ||
                prop.indexOf('vw') > -1 ||
                prop.indexOf('vh') > -1) {
        val = 16 * val
      }

      if (val === '') {
        val = prop
      }

      biggest = val > biggest ? val : biggest

      if (/[0-9.]/g.test(val)) {
        temp.push({
          viewSizePx: val,
          viewSizeStr: '',
          cssSize: prop,
          amount: fontSizes[prop]
        })
      } else {
        temp.push({
          viewSizePx: 0,
          viewSizeStr: val,
          cssSize: prop,
          amount: fontSizes[prop]
        })
      }
    }
  }

  r = temp.sort(function (a, b) {
    return b.viewSizePx - a.viewSizePx
  })

  return r
}

function getValueCount (values, property) {
  var arr = {}

  property = property || ''

  if (!values) {
    return arr
  }

  values.forEach(function (v) {
    v = v.replace(/['"]/g, '')

    if (arr.hasOwnProperty(v)) {
      arr[v] = arr[v] + 1
    } else {
      arr[v] = 1
    }
  })

  return arr
}
