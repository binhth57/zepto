
const UserAgent = (() => {
  const CodeEx = () => {
    console.log('CodeEx')
  }

  const itemClick = () => {
    let item = $('.card-header')
    item.on('click', (e) => {
      const ele = e.currentTarget
      if ($(ele).parents('.card').hasClass('is-open')) {
        $(ele).parents('.card').removeClass('is-open')
      } else {
        item.parents('.card').removeClass('is-open')
        $(ele).parents('.card').addClass('is-open')
      }
    })
  }

  $(document).ready(() => {
    let item = $('.card').first()
    item.addClass('is-open')
  })

  CodeEx()
  itemClick()
})()
export default UserAgent
