import { tns } from 'tiny-slider/src/tiny-slider'
import CallBackLazy from './LibCustom/CallBackLazy'
const SliderDemo = (() => {
  const modSlider = () => {
    let _this = '.mod-slider'
    let slider = '.slider'
    let sliderTns
    let elementTmp
    if ($(_this).length) {
      sliderTns = tns({
        container: slider,
        // items: 1,
        lazyload: false,
        slideBy: 'page',
        lazyloadSelector: '.lazy',
        autoplay: false,
        autoHeight: true,
        arrowKeys: true,
        // controls: true,
        mouseDrag: true,
        navPosition: 'bottom',
        controlsText: ['<span class="icomoon h1 icon-chevron-left"></span><span class="sr-only">Prev</span>', '<span class="icomoon h1 icon-chevron-right"></span><span class="sr-only">Next</span>'],
        responsive: {
          991: {
            // items: 1
          }
        }
      })
      sliderTns.events.on('transitionStart', (info, eventName) => {
        $(info.container).find('.lazy').each((index, element) => {
          elementTmp = element.tagName
          CallBackLazy.call(elementTmp, element)
          sliderTns.updateSliderHeight()
          $(info.container).find('.tns-slide-active .b-loaded').on('load', () => {
            sliderTns.updateSliderHeight()
          })
        })
      })
    }
  }
  modSlider()
})()
export default SliderDemo
