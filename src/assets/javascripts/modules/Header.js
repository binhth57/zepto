// import $ from 'jquery'
const Header = (() => {
  let _this = $('#header')
  let _numberScrol = 0
  if (_this.length) {
    const onResizeWindow = () => {
      settingPin()
    }
    const settingPin = () => {
      let scrollTop = document.documentElement.scrollTop
      if (scrollTop > _numberScrol) {
        _this.addClass('pin-header')
      } else {
        _this.removeClass('pin-header')
      }
    }
    const scrollPinHeader = () => {
      settingPin()
      $(window).on('scroll', () => {
        settingPin()
      })

      $(window).on('resize orientationchange', () => {
        onResizeWindow()
      })
    }
    scrollPinHeader()
  }
})()
export default Header
