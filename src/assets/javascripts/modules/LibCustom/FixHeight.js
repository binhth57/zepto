// import './ConvertHeight'
const FixHeight = (() => {
  const callFixHeight = () => {
    let _this = $('.fix-height')
    if (_this.length) {
      let $items = _this.find('.height-item').css('height', '')
      // $items.convertHeight()
      $items.matchHeight({
        byRow: true,
        property: 'height',
        target: null,
        remove: false
      })
    }
  }

  // $(window).on('resize orientationchange', () => {
  //   callFixHeight()
  // })

  callFixHeight()
})()
export default FixHeight
