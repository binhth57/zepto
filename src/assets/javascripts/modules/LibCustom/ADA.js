const ADA = (() => {
  const SkipToCotent = () => {
    $('.skip-link').on('click', () => {
      $('#main-content h1,#main-content a').eq(0).focus()
    })
  }
  const HiddenFocus = () => {
    $(document).on('click mousedown', (e) => {
      $('a,a *,button, button *,input,.checkmark-radio, .custom-tab,div,li, p,*').removeClass('mouse')
      $(e.target).addClass('mouse').parent().addClass('mouse')
      $(e.target).parents('a,.cont-bound ').addClass('mouse')
      $('.status-focus').removeClass('status-focus')
      $('.focus-status').removeClass('focus-status')
    })
  }
  const DectectFocus = () => {
    // $('#main-content').find('a,btn,input').focus(() => {
    //   $('.is-open-search').removeClass('is-open-search')
    // })
    // $('.main-menu-ul > li:not(.search-menu) a , .main-menu-ul > li.is-open-search a').focus(() => {
    //   $('.is-open-search').removeClass('is-open-search')
    // })
    $('.main-menu-ul > li > a').on('focusin', (e) => {
      $('.hovering').removeClass('hovering')
    })

    $(window).keydown((event) => {
      let focus = $('a:focus')
      if (event.keyCode === 40) {
        if (focus.parent().hasClass('has-sub')) {
          event.preventDefault()
          focus.parent().addClass('hovering')
        }
      }
    })

    $('a,button,input,div').on('focusin', (e) => {
      if (!$(e.target).parents('.tns-slider').length && !$(e.target).parents('.list-relate-mem ').length && !$(e.target).parents('.service-container').length && !$(e.target).parents('.industries-col').length && !$(e.target).parents('.mod-resources-widget').length && !$(e.target).parents('.social').length) {
        $('.focus-status').removeClass('focus-status')
      }
      if (!$(e.target).parents('.dropdown-select-c8').length) {
        $('.dropdown-select-c8').each((i, ele) => {
          $(ele).removeClass('show focus').find('.dropdown-menu').removeClass('show')
        })
      }
    })
  }
  const removetabindexdots = () => {
    setTimeout(() => {
      $('.tns-nav button').attr('tabindex', '0')
    }, 500)
  }
  const Setuptabindex = () => {
    let header = $('.header')
    header.attr('tabindex', '0').focus().removeAttr('tabindex')

    $('.tns-slider > div').each((i, ele) => {
      if (typeof $(ele).attr('id') === 'undefined') {
        $(ele).find('a,button').attr('tabindex', '-1')
      }
    })

    $('.control-tiny').each((index, ele) => {
      let tinyprev = $(ele).find('.tiny-prev')
      let tinynext = $(ele).find('.tiny-next')
      $(ele).removeAttr('tabindex')
      if (!$(ele).find('.tiny-next .line-arrow.d-none').length) {
        tinynext.attr('tabindex', '0')
      }
      if (!$(ele).find('.tiny-prev .line-arrow.d-none').length) {
        tinyprev.attr('tabindex', '0')
      }
      tinynext.append('<span class="sr-only">Enter to next item</span>')
      tinyprev.append('<span class="sr-only">Enter to previous item</span>')
    })
  }
  const InitADA = () => {
    $('h1').attr('tabindex', '0')
    $('.popup-is-open').removeAttr('tabindex')
    $('.tiny-prev,.tiny-next').keydown((e) => {
      if (e.keyCode === 13) {
        $(e.target).trigger('click').removeClass('mouse')
      }
    })
  }
  SkipToCotent()
  HiddenFocus()
  DectectFocus()
  removetabindexdots()
  Setuptabindex()
  InitADA()
})()
export default ADA
