// import $ from 'jquery'
const ConvertHeight = (($) => {
  $.fn.convertHeight = function () {
    let tmp = 0
    return this.each((index, el) => {
      let element = $(el)
      $(element).height('auto')
      let itemss = $(element)
      // console.log(element.offset().top)
      let innerHeights = itemss.map((index, el) => {
        return $(el).height()
      })
      let maxHeight = Math.max(...innerHeights)
      if (maxHeight > tmp) {
        tmp = maxHeight
      }
      element.height(tmp)
    })
  }
})($)
export default ConvertHeight
