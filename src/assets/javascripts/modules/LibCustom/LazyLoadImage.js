import lozad from 'lozad'
import CallConvertSvg from './ConvertSvg'
const LazyLoadImage = (() => {
  const lazyLoadImage = () => {
    let time
    let lazyimage = '.lazy'
    if ($(lazyimage).length) {
      const observer = lozad(lazyimage, {
        threshold: 0.1,
        load: (element) => {
          if (element.tagName === 'IMG') {
            element.src = element.getAttribute('data-src')
          } else {
            let bg = element.getAttribute('data-src')
            $(element).css({
              backgroundImage: 'url("' + bg + '")'
            })
          }
          $(element).addClass('b-loaded').removeClass('lazy').attr('data-loaded', 'true').removeAttr('data-src')
          let sliderLazy = $(element).parents('.slider-lazy')

          if (sliderLazy.hasClass('tns-slider')) {
            let prevActive = sliderLazy.find('.tns-slide-active').prev().find('.lazy')
            let nextActive = sliderLazy.find('.tns-slide-active').next().find('.lazy')
            let srcprev = prevActive.attr('data-src')
            let srcnext = nextActive.attr('data-src')
            if (prevActive.length) {
              if (prevActive[0].nodeName === 'IMG') {
                prevActive.attr('src', srcprev)
              } else {
                prevActive.css({
                  'background-image': 'url(' + srcprev + ')'
                })
              }
            }
            if (nextActive.length) {
              if (nextActive[0].nodeName === 'IMG') {
                nextActive.attr('src', srcnext)
              } else {
                nextActive.css({
                  'background-image': 'url(' + srcnext + ')'
                })
              }
            }

            prevActive.removeClass('lazy').addClass('b-loaded')
            nextActive.removeClass('lazy').addClass('b-loaded')
          }

        // set height div when img load done
          if ($(element)[0].tagName === 'IMG') {
            time = setTimeout(() => {
              clearTimeout(time)
              if ($(element).parents('.fix-height').length) {
                window.callFixHeight()
              }
            }, 1000)
            $(element).on('load', () => {
              $(element).on('load', () => {
                if ($(element).parents('.fix-height').length) {
                  window.callFixHeight()
                }
              }, 200)
            })
          }

          setTimeout(() => {
            CallConvertSvg.convertSvg()
          }, 150)
        }
      })
      observer.observe()
    }
  }

  lazyLoadImage()
  $(window).on('resize orientationchange', () => {
    lazyLoadImage()
  })
})()
export default LazyLoadImage
