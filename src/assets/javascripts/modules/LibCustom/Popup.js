// import $ from 'jquery'
// import select from './SelectC8'
// import {
//   tns
// } from 'tiny-slider/src/tiny-slider'
const Popup = (() => {
  const $html = $('html')
  const $modPop = '.mod-popup'
  // const $modPopStatic = '.mod-popup-static'
  const $ClosePopup = '.popup-is-close'
  const $openPopup = '.popup-is-open'
  let closeContent = '<span class="icomoon icon-close"></span><span class="sr-only">Close Popup</span>'
  let contentInner = '<div class="popup-inner"><h3>The requested content cannot be loaded. <br> Please try again later.</h3></div>'
  let widthContent = 'container'
  let widthContentTmp = widthContent
  let iframe = ''
  // let gallarySlider
  // let initialSlideTMp = 0
  // if ($($ClosePopup).length) {
  const renderPopup = () => {
    let html = '<div class="mod-popup popup-show">' +
      '<div class="popup-container ps-as">' +
      '<div class="popup-content ' + widthContent + '">' +
      ' <div class="mask-pop-overlay"></div>' +
      '</div>' +
      '</div>' +
      '</div>'
    $(document.body).append(html)
  }

  const openPopupGallery = () => {
    $html.on('click', $openPopup, (e) => {
      // cal lazy img into popup
      LoadLazyOnLoad()
      const ele = e.currentTarget
      let cloneTmp = ''
      $(ele).addClass('is-click')
      let htmlClass = $(ele).data('htmlclass')
      let tmpContent = $(ele).data('id')
      let tmpPopup = $(ele).data('popup')
      let tmpWidthContent = $(tmpContent).data('content')
      if ($(tmpContent).hasClass('mod-popup-static')) {
        $(tmpContent).addClass('popup-static')
      } else {
        if (typeof tmpWidthContent !== 'undefined') {
          widthContent = tmpWidthContent
        }
        // popup video
        if (tmpPopup === 'video') {
          let srcVideo = $(ele).attr('href')
          srcVideo = convertToEmbed(srcVideo)
          srcVideo = addMutedVideo(srcVideo)
          iframe = '<div class="popup-inner popup-video "><div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="' + srcVideo + '" frameborder="0"></iframe></div></div>'
          renderPopup()
          let $popupContent = $($modPop).find('.popup-content')
          $popupContent.append(iframe)
          $popupContent.find('.popup-inner').append('<a href="javascript:;" class="popup-is-close text-decoration-none">' + closeContent + '</a>')
        } else if (tmpPopup === 'garally') {
          // popup garally
          cloneTmp = $(tmpContent).find('.popup-inner').clone()
          renderPopup()
          let $popupContent = $($modPop).find('.popup-content')
          if ($(tmpContent).length) {
            $popupContent.append(cloneTmp)
            // gallarySlider = '.popup-content .gallary-silder'
            setTimeout(() => {
              callSliderGallery()
            }, 100)
          } else {
            $popupContent.addClass('no-element').append(contentInner)
          }
          $popupContent.find('.popup-inner').append('<a href="javascript:;" class="popup-is-close text-decoration-none">' + closeContent + '</a>')
        } else {
          // popup content
          cloneTmp = $(tmpContent).find('.popup-inner').clone()
          renderPopup()
          let $popupContent = $($modPop).find('.popup-content')
          if ($(tmpContent).length) {
            $popupContent.append(cloneTmp)
          } else {
            $popupContent.addClass('no-element').append(contentInner)
          }
          $popupContent.find('.popup-inner').append('<a href="javascript:;" class="popup-is-close text-decoration-none">' + closeContent + '</a>')
        }
      }
      if ($(tmpContent).hasClass('popup-static')) {
        $(tmpContent).addClass('popup-show')
      } else {
        $($modPop).addClass('popup-show')
      }
      $html.addClass(htmlClass).addClass('popup-open')

      setTimeout(() => {
        $html.addClass('popup-animation')
      }, 100)
      // callBack()
      return false
    })
  }

  const clickClosePopup = () => {
    $html.on('click', $ClosePopup, (e) => {
      closePopup()
    })
  }

  const closePopup = () => {
    let classHtml = $html.find('.is-click').data('htmlclass')
    $html.removeClass('popup-open popup-animation ' + classHtml)
    if ($html.find('.popup-show').hasClass('popup-static')) {
      $html.find('.popup-static').removeClass('popup-show')
    } else {
      $($modPop).remove()
    }
    $html.find('.is-click').removeClass('is-click')
    widthContent = widthContentTmp
  }

  const clickOutSite = () => {
    $html.on('click', '.mask-pop-overlay', (e) => {
      closePopup()
    })
    $(window).keydown((e) => {
      if (e.keyCode === 27 && $('.mod-popup').length) {
        closePopup()
      }
      if (e.keyCode === 13 && $(e.target).hasClass('popup-is-close')) {
        closePopup()
      }
    })
  }

  const callSliderGallery = () => {
    // tns({
    //   container: gallarySlider,
    //   // items: 1,
    //   lazyload: true,
    //   slideBy: 'page',
    //   lazyloadSelector: '.lazy',
    //   autoplay: false,
    //   autoHeight: true,
    //   arrowKeys: true,
    //   // controls: true,
    //   mouseDrag: true,
    //   navPosition: 'bottom',
    //   controlsText: ['<span class="icomoon h1 icon-chevron-left"></span><span class="sr-only">Prev</span>', '<span class="icomoon h1 icon-chevron-right"></span><span class="sr-only">Next</span>']
    // })
  }

  const LoadLazyOnLoad = () => {
    let hasLazy = $('.popup-inner .lazy')
    if (hasLazy.length) {
      hasLazy.each((index, el) => {
        let srclazy = $(el).attr('data-src')
        if ($(el)[0].tagName === 'IMG') {
          $(el).attr('src', srclazy)
        } else {
          $(el).css({
            'background-image': 'url(' + srclazy + ')'
          })
        }
        $(el).removeClass('lazy').addClass('b-loaded')
      })
    }
  }
  const convertToEmbed = (url) => {
    if (url.toLowerCase().indexOf('youtube.com') !== -1 && url.toLowerCase().indexOf('youtube.com/embed') === -1) {
      let tempV = url.split('?v=')[1]
      let v = tempV.split('&')[0]
      return 'https://www.youtube.com/embed/' + v + '?autoplay=1&rel=0&showinfo=0'
    } else
    if (url.toLowerCase().indexOf('vimeo.com') !== -1 && url.toLowerCase().indexOf('player.vimeo.com/video') === -1) {
      let parts = url.split('/')
      let v2 = parts.pop()
      return 'https://player.vimeo.com/video/' + v2 + '?autoplay=1&muted=0'
    } else
    if (url.toLowerCase().indexOf('youtu.be') !== -1) {
      let parts2 = url.split('/')
      let v3 = parts2.pop()
      return 'https://www.youtube.com/embed/' + v3 + '?autoplay=1&rel=0&showinfo=0'
    }

    return url
  }

  const addMutedVideo = (url) => {
    if (url.toLowerCase().indexOf('youtube.com') === -1) {
      if (url.toLowerCase().indexOf('?') === -1) {
        return url + '?muted=0'
      } else {
        return url + '&muted=0'
      }
    }
    return url
  }

  openPopupGallery()
  closePopup()
  clickOutSite()
  clickClosePopup()

  return {
    renderPopup: renderPopup,
    openPopupGallery: openPopupGallery,
    clickClosePopup: clickClosePopup,
    closePopup: closePopup,
    clickOutSite: clickOutSite,
    callSliderGallery: callSliderGallery
  }
  // }
})()
export default Popup
