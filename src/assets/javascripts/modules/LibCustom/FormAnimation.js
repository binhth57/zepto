const FormAnimation = (() => {
  const formAnimation = () => {
    const formGroup = '.form-group-v2'
    const formControl = 'input.form-control'
    const activeFocus = 'active-focus'
    const hasFormAni = $('.form-ani')
    if (hasFormAni.length) {
      hasFormAni.find(formGroup).on('focus', formControl, (e) => {
        const ele = e.currentTarget
        $(formControl).each((index, el) => {
          if ($(el)[0].value.length < 1) {
            $(el).parents(formGroup).removeClass(activeFocus)
          }
        })
        $(ele).parents(formGroup).addClass(activeFocus)
      })
      hasFormAni.find(formGroup).on('change', formControl, (e) => {
        const ele = e.currentTarget
        $(formControl).each((index, elc) => {
          if ($(elc)[0].value.length < 1) {
            $(elc).parents(formGroup).removeClass(activeFocus)
          }
        })
        $(ele).parents(formGroup).addClass(activeFocus)
      })

      hasFormAni.find(formControl).each((index, el) => {
        if ($(el)[0].value.length) {
          $(el).parents(formGroup).addClass(activeFocus)
        }
      })

      $(document).on('click', (e) => {
        const ele = e.target
        if (!$(ele).is('.form-group-v2 .form-control, .form-group-v2 .form-control')) {
          $(formControl).each((index, el) => {
            if ($(el)[0].value.length < 1) {
              $(el).parents(formGroup).removeClass(activeFocus)
            }
          })
        }
      })
    }
  }

  formAnimation()
})()
export default FormAnimation
