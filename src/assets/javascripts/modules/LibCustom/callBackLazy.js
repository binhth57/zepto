const CallBackLazy = (() => {
  let datasrc
  const call = (elementTmp, element) => {
    datasrc = element.getAttribute('data-src')
    if (elementTmp === 'IMG') {
      element.setAttribute('src', datasrc)
    } else {
      $(element).css({
        'background-image': 'url(' + datasrc + ')'
      })
    }
    $(element).addClass('b-loaded').removeClass('lazy').attr('data-loaded', 'true')
  }
  return {
    call: call
  }
})()
export default CallBackLazy
