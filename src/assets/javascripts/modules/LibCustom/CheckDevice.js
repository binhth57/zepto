const CheckDevice = (() => {
  const $html = $('html')
  const checkDevice = () => {
    const device = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)
    if (device) {
      $html.addClass('touch')
    } else {
      $html.addClass('no-touch')
    }
  }
  checkDevice()
})()
export default CheckDevice
