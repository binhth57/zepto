const ConvertSvg = (() => {
  const convertSvg = () => {
    let hasSvg = $('img.img-svg:not(.lazy)')
    if (hasSvg.length) {
      hasSvg.each((index, e) => {
        let $img = $(e)
        let imgID = $img.attr('id')
        let imgClass = $img.attr('class')
        let imgURL = $img.attr('src')
        if (imgURL !== null) {
          $.get(imgURL, (data) => {
            let $svg = $(data).find('svg')
            if (typeof imgID !== 'undefined') {
              $svg = $svg.attr('id', imgID)
            }
            if (typeof imgClass !== 'undefined') {
              $svg = $svg.attr('class', imgClass + ' replaced-svg')
            }
            $svg = $svg.removeAttr('xmlns:a')
            if (!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
              $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
            }
            if ($svg.length) {
              $img.replaceWith($svg)
            }
          }, 'xml')
        }
      })
    }
  }

  convertSvg()
  return {
    convertSvg: convertSvg
  }
})()
export default ConvertSvg
