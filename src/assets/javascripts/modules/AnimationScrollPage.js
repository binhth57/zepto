const AnimationScrollPage = (() => {
  const $overLoader = $('.over-loader')
  let $elems
  let winH
  let winW
  let offset
  let wintop
  let topcoords
  const removeLoading = () => {
    animationEle()
  }
  const animationEle = () => {
    $elems = $('.animation')
    winH = window.innerHeight
    winW = window.innerWidth
    offset = winH - (winH / 13)
    if (winW > 991 && !$('body').hasClass('no-animation')) {
      wintop = $(window).scrollTop()
      $elems.each((index, ele) => {
        let $elm = $(ele)
        if ($elm.hasClass('set-animation')) {
          return true
        }
        topcoords = $elm.offset().top
        if (wintop > (topcoords - offset)) {
          $elm.addClass('set-animation')
        }
      })
    }
  }

  $(window).on('scroll resize orientationchange', () => {
    animationEle()
  })

  if ($overLoader.length) {
    removeLoading()
  } else {
    animationEle()
  }
  window.animationEle = animationEle
})()
export default AnimationScrollPage
