// Js custom
import 'modules/LibCustom/matchHeight'
import 'modules/LibCustom/FixHeight'
import 'modules/LibCustom/Popup'
import 'modules/LibCustom/SelectC8'
import 'modules/LibCustom/BrowserDetection'
import 'modules/LibCustom/CheckDevice'
import 'modules/LibCustom/ConvertSvg'
import 'modules/LibCustom/LazyLoadImage'
import 'modules/LibCustom/FormAnimation'
import 'modules/LibCustom/ADA'

import 'modules/UserAgent'
import 'modules/Header'
import 'modules/Menu'
// import 'modules/AnimationPage'
import 'modules/AnimationScrollPage'
// import 'modules/SliderDemo'

import 'modules/ModFeature'
import 'modules/ModPortfolio'
import 'modules/ModPartner'
import 'modules/ModAbout'
import 'modules/ModRelated'

console.log('App Desktop')
