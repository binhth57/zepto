module.exports =
{
  title: 'Internal Page Title',
  desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer non porta nisi, vel tempor velit. Etiam vel efficitur diam, id dignissim ligula. Praesent a lacus eu elit maximus porttitor. In maximus consectetur nunc eget lacinia.',
  btnContent: 'Schedule a Demo',
  backgroundImg: '/assets/images/title.jpg'
}
