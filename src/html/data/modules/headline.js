module.exports = {
  title: 'Headline for a section of content here that’s centered',
  subTitle: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer non porta nisi, vel tempor velit. Etiam vel efficitur diam, id dignissim ligula. Praesent a lacus eu elit maximus porttitor.',
  btn: 'Optional CTA',
  slide: [
    {
      title: 'Benefit or text headline for a section of content',
      image: '/assets/images/headline-1.jpg',
      desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer non porta nisi, vel tempor velit. Etiam vel efficitur diam, id dignissim ligula.'
    },
    {
      title: 'Benefit or text headline for a section of content',
      image: '/assets/images/headline-2.jpg',
      desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer non porta nisi, vel tempor velit. Etiam vel efficitur diam, id dignissim ligula.'
    },
    {
      title: 'Benefit or text headline for a section of content',
      image: '/assets/images/headline-3.jpg',
      desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer non porta nisi, vel tempor velit. Etiam vel efficitur diam, id dignissim ligula.'
    }
  ]
}