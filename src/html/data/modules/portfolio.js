module.exports = {
  title: 'Headline for a section of content here that’s centered',
  subTitle: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer non porta nisi, vel tempor velit. Etiam vel efficitur diam, id dignissim ligula. Praesent a lacus eu elit maximus porttitor. In maximus.',
  btn: 'Optional CTA',
  slide: [
    {
      image: '/assets/images/module.jpg'
    },
    {
      image: '/assets/images/module.jpg'
    },
    {
      image: '/assets/images/module.jpg'
    }
  ]
}
