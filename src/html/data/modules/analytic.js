module.exports = {
  title: 'Global risk exchange and advanced analytics on a single platform',
  image: '/assets/images/module.jpg',
  tab: [
    {
      title: 'CyberGRX Exchange',
      image: '/assets/images/module.jpg',
      desc: 'Reduce risk with our two-sided marketplace of assessments, third-party risk data, dynamic threat intelligence and industry-relevant cyber risk benchmarks.'
    },
    {
      title: 'AIR Insights™',
      image: '/assets/images/module.jpg',
      desc: 'Reduce risk with our two-sided marketplace of assessments, third-party risk data, dynamic threat intelligence and industry-relevant cyber risk benchmarks.'
    },
    {
      title: 'Dynamic Assessment',
      image: '/assets/images/module.jpg',
      desc: 'Reduce risk with our two-sided marketplace of assessments, third-party risk data, dynamic threat intelligence and industry-relevant cyber risk benchmarks.'
    },
    {
      title: 'Residual Risk Profiles',
      image: '/assets/images/module.jpg',
      desc: 'Reduce risk with our two-sided marketplace of assessments, third-party risk data, dynamic threat intelligence and industry-relevant cyber risk benchmarks.'
    }
  ]
}