module.exports = [
  {
    title: 'Left Text / Right Image',
    image: '/assets/images/information-1.jpg',
    subTitle: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque id laoreet odio.',
    desc: 'Aenean eget purus fermentum, ornare ante vitae, malesuada orci. Maecenas in neque id lacus tristique consectetur. Aenean eget malesuada leo, nec semper augue. Morbi laoreet, turpis non posuere imperdiet, tortor odio convallis purus, in malesuada quam urna vel risus.'
  },
  {
    title: 'Left Text / Right Image',
    image: '/assets/images/information-2.jpg',
    subTitle: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque id laoreet odio.',
    desc: 'Aenean eget purus fermentum, ornare ante vitae, malesuada orci. Maecenas in neque id lacus tristique consectetur. Aenean eget malesuada leo, nec semper augue. Morbi laoreet, turpis non posuere imperdiet, tortor odio convallis purus, in malesuada quam urna vel risus.'
  }
]
