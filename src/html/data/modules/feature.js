module.exports = [
  {
    title: 'Innovative, Dynamic Risk Assessment Solution',
    image: '/assets/images/slider.jpg',
    desc: 'CyberGRX Exchange isn’t just another application in your security posture, it’s an innovative software solution that guides you through the TPCRM journey. From ingesting third parties, applying advanced and predictive analytics, suggesting risk reduction and continuously monitoring the ecosystem, our platform keeps you informed, in control and in compliance.'
  },
  {
    title: 'Innovative, Dynamic Risk Assessment Solution',
    image: '/assets/images/slider.jpg',
    desc: 'CyberGRX Exchange isn’t just another application in your security posture, it’s an innovative software solution that guides you through the TPCRM journey. From ingesting third parties, applying advanced and predictive analytics, suggesting risk reduction and continuously monitoring the ecosystem, our platform keeps you informed, in control and in compliance.'
  },
  {
    title: 'Innovative, Dynamic Risk Assessment Solution',
    image: '/assets/images/slider.jpg',
    desc: 'CyberGRX Exchange isn’t just another application in your security posture, it’s an innovative software solution that guides you through the TPCRM journey. From ingesting third parties, applying advanced and predictive analytics, suggesting risk reduction and continuously monitoring the ecosystem, our platform keeps you informed, in control and in compliance.'
  }
]
