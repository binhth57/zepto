module.exports = {
  title: 'Ready to Talk Pricing?',
  subTitle: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque id laoreet odio. Vivamus in justo neque. Vivamus aliquet consectetur nisi, vel lobortis massa hendrerit ac.',
  btn: 'Call to Action',
  options: [
    {
      country: 'Vietnam'
    },
    {
      country: 'Japan'
    },
    {
      country: 'England'
    }
  ]
}