module.exports = {
  normal: [
    {
      title: 'Benefit or text headline for a section of content',
      image: '/assets/images/slider.jpg',
      desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer non porta nisi, vel tempor velit.'
    },
    {
      title: 'Benefit or text headline for a section of content',
      image: '/assets/images/slider.jpg',
      desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer non porta nisi, vel tempor velit.'
    },
    {
      title: 'Benefit or text headline for a section of content',
      image: '/assets/images/slider.jpg',
      desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer non porta nisi, vel tempor velit.'
    },
    {
      title: 'Benefit or text headline for a section of content',
      image: '/assets/images/slider.jpg',
      desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer non porta nisi, vel tempor velit.'
    }
  ],
  large: [
    {
      title: 'Benefit or text headline for a section of content',
      image: '/assets/images/slider.jpg',
      desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer non porta nisi, vel tempor velit. Etiam vel efficitur diam, id dignissim ligula. Praesent a lacus eu elit maximus porttitor. In maximus consectetur nunc eget lacinia.'
    },
    {
      title: 'Benefit or text headline for a section of content',
      image: '/assets/images/slider.jpg',
      desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer non porta nisi, vel tempor velit. Etiam vel efficitur diam, id dignissim ligula. Praesent a lacus eu elit maximus porttitor. In maximus consectetur nunc eget lacinia.'
    }
  ]
}