module.exports = {
  infor: [
    {
      image: '/assets/images/avatar.jpg',
      name: 'Adam Fletcher',
      job: 'CISO of Blackstone',
      btn: 'View Full Case Study',
      desc: 'CyberGRX is a force multiplier for our third-party cyber risk management program. In just the first year we will be able to assess 3x more vendors than we assessed last year and reallocate the resources saved to true risk management and mitigation efforts.'
    },
    {
      image: '/assets/images/avatar.jpg',
      name: 'Adam Flatcher',
      job: 'CISO of Blackstone',
      btn: 'View Full Case Study',
      desc: 'CyberGRX is a force multiplier for our third-party cyber risk management program. In just the first year we will be able to assess 3x more vendors than we assessed last year and reallocate the resources saved to true risk management and mitigation efforts.'
    },
    {
      image: '/assets/images/avatar.jpg',
      name: 'Adam Flytcher',
      job: 'CISO of Blackstone',
      btn: 'View Full Case Study',
      desc: 'CyberGRX is a force multiplier for our third-party cyber risk management program. In just the first year we will be able to assess 3x more vendors than we assessed last year and reallocate the resources saved to true risk management and mitigation efforts.'
    }
  ],
  logos: [
    {
      image: '/assets/images/partner-1.jpg'
    },
    {
      image: '/assets/images/partner-2.jpg'
    },
    {
      image: '/assets/images/partner-3.jpg'
    },
    {
      image: '/assets/images/partner-4.jpg'
    }
  ]
}