module.exports = {
  title: 'Trusted & Recommended',
  logos: [
    {
      title: 'Logo',
      image: '/assets/images/partner-1.jpg'
    },
    {
      title: 'Logo',
      image: '/assets/images/partner-2.jpg'
    },
    {
      title: 'Logo',
      image: '/assets/images/partner-3.jpg'
    },
    {
      title: 'Logo',
      image: '/assets/images/partner-4.jpg'
    },
    {
      title: 'Logo',
      image: '/assets/images/partner-5.jpg'
    },
    {
      title: 'Logo',
      image: '/assets/images/partner-6.jpg'
    }
  ]
}