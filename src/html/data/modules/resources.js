module.exports = {
  title: 'Ready to see CyberGRX Exchange in action?',
  subTitle: 'Find out how CyberGRX transforms TCPRM for enterprises and third parties.',
  btn1: 'Free Trial',
  btn2: 'Request a Demo',
  titleRight: 'Resources',
  link1: 'Third-Party Cyber Risk Management 101',
  link2: 'How to Justify the Cost to Your Manager',
  link3: 'Third-Party Vendor? Here’s What you Need to Know'
}