module.exports = {
  title: require('./modules/title'),
  information: require('./modules/information'),
  feature: require('./modules/feature'),
  portfolio: require('./modules/portfolio'),
  headline: require('./modules/headline'),
  post: require('./modules/post'),
  analytic: require('./modules/analytic'),
  partner: require('./modules/partner'),
  about: require('./modules/about'),
  resources: require('./modules/resources'),
  related: require('./modules/related'),
  contact: require('./modules/contact')
}
